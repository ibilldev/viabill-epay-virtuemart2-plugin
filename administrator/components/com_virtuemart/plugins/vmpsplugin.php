<?php

defined ('_JEXEC') or die('Restricted access');
/*================================MODIFIED TO PLACE PRICE TAG CODE FOR VIABILL EPAY=====================================================*/
if (!class_exists ('vmPlugin')) {
	require(VMPATH_PLUGINLIBS . DS . 'vmplugin.php');
}

abstract class vmPSPlugin extends vmPlugin {	


//================================COPY THIS FUNCTION INTO YOUR FILE=======================================================================
	protected function getPluginHtml ($plugin, $selectedPlugin, $pluginSalesPrice) {

		$pluginmethod_id = $this->_idName;
		$pluginName = $this->_psType . '_name';
		//echo "<pre>";
		//print_r($plugin->viabillepay_vbpricetag);
		
		if ($selectedPlugin == $plugin->$pluginmethod_id) {
			$checked = 'checked="checked"';
		} else {
			$checked = '';
		}

		if (!class_exists ('CurrencyDisplay')) {
			require(JPATH_VM_ADMINISTRATOR . DS . 'helpers' . DS . 'currencydisplay.php');
		}
		$currency = CurrencyDisplay::getInstance ();
		$costDisplay = "";
		if ($pluginSalesPrice) {
			$costDisplay = $currency->priceDisplay ($pluginSalesPrice);
			$costDisplay = '<span class="' . $this->_type . '_cost"> (' . JText::_ ('COM_VIRTUEMART_PLUGIN_COST_DISPLAY') . $costDisplay . ")</span>";
		}

			/*---------------CODE FOR  VIABILL PRICE TAG-------------------------*/
		 	$plugin_element = $this->_psType . '_element';
			$cart = VirtueMartCart::getCart();
			

			$showpricetag = false;
			if($plugin->$plugin_element =='viabillepay' && $plugin->viabillepay_vbpricetag!=null ){

				$showpricetag =true;
				 ?>
				 	<script type="text/javascript" >
				 			<?php  echo $plugin->viabillepay_vbpricetag; ?>
				 	</script>
				 <?php
			}			

				

			 $cartPriceQP = $cart->pricesUnformatted['billTotal'] ;
			 $plugid = $plugin->$pluginmethod_id;

		if( $showpricetag ==true){
				$pricetagpluginName ='<div id="defaulttext" class="no-pricetag"><div class="viabill-pricetag" view="payment" id="checkpricetag_'.$plugid.'"  price="'. $cartPriceQP.'">'.$plugin->$pluginName.'</div></div></span>';
				$script = "<script type='text/javascript'>						
							
							var priceqp = '$cartPriceQP';
							var plgid = '$plugid';						
							
							setTimeout(function(){
								
							if(vb.isLow(priceqp)  || vb.isHigh(priceqp)){
							jQuery('#defaulttext').removeClass('no-pricetag');		
							}
							else{
								
								jQuery('#defaulttext').html('".$plugin->$pluginName."');
								jQuery('#defaulttext').addClass('no-pricetag');
							}	
								}, 1000);

							</script>";


		$html = '<input style="vertical-align:top; padding-top:15px;" type="radio"'.$dynUpdate.' name="' . $pluginmethod_id . '" id="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '"   value="' . $plugin->$pluginmethod_id . '" ' . $checked . ">\n"
			. '<label for="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '">' . '<span class="' . $this->_type . '">' . $script. $pricetagpluginName.  $costDisplay . "</span></label>\n";

			$html.="<style>
				.vb-payment-text{
						height :4px !important;
				}
				.vb-payment-text{
					 margin-top: -12px;	padding-left: 18px;

				}
				.no-pricetag{
					margin-top: -12px;	padding-left: 18px;
					height :4px !important;
				}
				</style>";

		}else{


				$html = '<input type="radio"'.$dynUpdate.' name="' . $pluginmethod_id . '" id="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '"   value="' . $plugin->$pluginmethod_id . '" ' . $checked . ">\n"
			. '<label for="' . $this->_psType . '_id_' . $plugin->$pluginmethod_id . '">' . '<span class="' . $this->_type . '">' . $plugin->$pluginName . $costDisplay . "</span></label>\n";

		}

			/*-----------------------------------------PRICE TAG END---------------------------------------------------------------------------------------------*/	
		return $html;
	} //end of function getPluginHtml()

//===============================================================================================================================================================
}//end of class

