#Virtuemart -  ViaBill ePay Module  #
----------------------------
A module to be integrated in Joomla Virtuemart for providing Payment Option and not a Payment Gateway.
ViaBill is a Payment Method and not a Payment Gateway.

###Facts###
-----
- version: 1.0
- Module on BitBucket(https://pdviabill@bitbucket.org/ibilldev/viabill-epay-virtuemart2.git)


###Description###
-----------
Pay using ViaBill. 
Install this Module in to your Virtuemart to provide an option to pay using ViaBill through ePay Payment Gateway.

###Requirements###
------------
* PHP >= 5.5.0


###Compatibility###
-------------
* Virtuemart = 2
* Joomla >=2.0

###Integration Instructions :  ### 
-------------------------
1. Download the Module from the bitbucket. 
2. Extract the module and look for a zip file viz. vm2-viabillepay
3. Login to the Joomla Admin Panel. Navigate to the menu Extensions->Extension Manager -> Install.
4. Upload Package File : Find the file you just downloaded via “Upload Package File”, and press ‘Upload & Install’ 
   Please check the image below for reference :
![virtuemart-browse-epay.PNG](https://bitbucket.org/repo/8dbpr6/images/328528260-virtuemart-browse-epay.PNG)

![virtuemart-enable-viabillepay.PNG](https://bitbucket.org/repo/8dbpr6/images/3241513739-virtuemart-enable-viabillepay.PNG)

5. Once upload is successful, Go to Components -> VirtueMart -> Payment Methods. Press ‘New’ in the top right corner, and enter your information(please see image)   Press ‘Save’ in the top right corner.
6. Press ‘Configuration’ on the right (see image). Enter your merchant number in the field “Merchant number”. It’s either a number for tests or production.

![vm-viabillepay-configure.PNG](https://bitbucket.org/repo/8dbpr6/images/3730886958-vm-viabillepay-configure.PNG)

7. Done

8. Inside the downloaded module look for the folder named administrator. Open the file components/com_virtuemart/plugins/vmpsplugin.php

9. Copy the complete code inside the function named : protected function getPluginHtml ($plugin, $selectedPlugin, $pluginSalesPrice)  and paste it into your existing function in the file in administrator/components/com_virtuemart/plugins/vmpsplugin.php.  
Note :  Replace the function protected function getPluginHtml ($plugin, $selectedPlugin, $pluginSalesPrice). 

10. Save and Close.




#Uninstallation/Disable the Module
-------------------

1. Login to the Joomla Admin Panel. Navigate to the menu Extensions -> Extension Manager -> Manage

2. Look for VMPAYMENT_VIABILLEPAY and then change the status to disable it.
3. To Uninstall : On the Same page :  Select the plugin and click Uninstall on the top right croner.

![disable-vm-viabillepay.PNG](https://bitbucket.org/repo/8dbpr6/images/3106154605-disable-vm-viabillepay.PNG)




#Support
-------
If you have any issues with this extension, kindly drop us a mail on [support@viabill.com](mailto:support@viabill.com)

#Contribution
------------

#Developer
---------


#License
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

#Copyright
---------
(c) 2016 ViaBill